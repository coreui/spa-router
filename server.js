var express = require('express'),
    path = require('path');
    app = express();

app.use('/js', express.static(
    path.resolve(__dirname, 'frontend', 'js'),
    {extensions: ["js"]}
));

app.get('/*', function(req, res){

    res.sendFile(path.resolve(__dirname, 'frontend', 'index.html'));
});


app.listen(process.env.PORT || 3000, function(){
    
    console.log('Server running on port 3000');
});