//temp route registery
var oRoutes = {
    "/": {
        "_asImports": [
            `<header>
                <h1>CUI2 - Dashboard</h1>
                <hr>
            </header>
            <nav>
                <ul>
                    <li><a href="/" title="CUI2 Landing Home" data-route>Home</a></li>
                    <li><a href="/parcel" title="Parcel" data-route>Parcel</a></li>
                    <li><a href="/reports" title="Reports" data-route>Reports</a></li>
                    </li>
                </ul>
            </nav>
            <main>
                <div id="body-container">
                </div>
            </main>`
        ],
        "login": {
            "_asImports": [
                `<header>
                    <h1>CUI2 - Login</h1>
                    <hr>
                </header>
                <main>
                    <div id="body-container">
                        <h2>Please Login</h2>
                        <button><a href="/" title="Login" data-route>Login</a></button>
                    </div>
                </main>`
            ]
        },
        "parcel": {    
            "_asImports": [
                `<header>
                        <h1>CUI2 - Parcels View</h1>
                        <hr>
                    </header>
                    <nav>
                        <ul>
                            <li><a href="/parcel/owners" title="Parcel-Owners" data-route>Owners</a></li>
                            <li><a href="/parcel/search" title="Search" data-route>Search</a></li>
                            </li>
                        </ul>
                    </nav>
                    <main>
                        <div id="body-container">
                            <p>NYS Parcel View</p>
                            <label>County:</label>
                            <select>
                                <option></option>
                                <option>Some County</option>
                                <option>Some More County</option>
                                <option>Another County</option>
                                <option>Another One (TM)</option>
                            </select>
                        </div>
                    </main>`
            ],
            "owners": { 
                "_asImports": [
                    `<h1>Owners - Module</h1>`
                ]
            },
            "search":{
                "_asImports": 
                [
                    `<h1>Search - Module</h1>`
                ]
            }   
        },    
        "reports": {
            "_asImports": [
                `<h1>Report Module</h1>
                <table border="1">
                    <tr>
                        <th>ID</th>
                        <th>Filed</th>
                        <th>Processed</th>
                    </tr>
                    <tr>
                        <td>0123456789</td>
                        <td>Yes</td>
                        <td>No</td>
                    </tr>
                    <tr>
                        <td>987654321</td>
                        <td>No</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>0123456789</td>
                        <td>Yes</td>
                        <td>No</td>
                    </tr>
                    <tr>
                        <td>987654321</td>
                        <td>No</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>0123456789</td>
                        <td>Yes</td>
                        <td>No</td>
                    </tr>
                    <tr>
                        <td>987654321</td>
                        <td>No</td>
                        <td></td>
                    </tr>
                </table>`
            ]
        },
        "404": {
            "_asImports": [
                `<h1 style="text-align:center;padding-top:150px">404 ERROR!!<br/>PAGE NOT FOUND</h1>`
            ]
        }
    }
};

export {oRoutes};