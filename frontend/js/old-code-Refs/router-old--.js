var _priv = {
    bMatchedRoute: false,
    o404Route: undefined,
    sPrevScreen : undefined
},
_events = {},

_ids = {
    wrapper: "body-container"
},

_attrs = {
    route: "[data-route]", //route links with this attr
    activeRoute: "[data-route-active]"
};

//route link click listener
_events.RouteLinkClick = function(evt){

var dTarget = evt.target;

if(dTarget.matches(_attrs.route)){

    evt.preventDefault();

    _priv.sPrevRoute = dTarget.href;
    
    _priv.nagivate(dTarget.href);
}
};

_priv.nagivate = function(url){

//update browser session history stack
history.pushState(null, null, url);

_priv.router();
};

//get 404 route so it can it easily referenced
_priv.get404Route = function(){

for(var route in oRoutes){
    
    //second level route search
    for(var screen in oRoutes[route]){

        if(screen == '404'){

            var o404Route = oRoutes[route][screen];

            _priv.o404Route = o404Route._asImports;

            break;
        }
    }
}
};

_priv.getNestedRoutes = function(oRoutes, aPathnames, oResult){

var n = 0;

var findNestedRoute = function(oRoutes){

    for(var route in oRoutes){

        if(route == aPathnames[n] && typeof oRoutes[route] == 'object'){

            var oPath = {
                sName: aPathnames[n],
                _asImports: oRoutes[route]._asImports
            };

            oResult._asImports.push(oPath);

            n++;

            findNestedRoute(oRoutes[route]);
        }
    }
};

findNestedRoute(oRoutes);
};

_priv.renderRouteImports = function(oRouteResult){

console.log('Previous screen:',_priv.sPrevScreen);

var bRouteIncludesPrevScreen = false;

//nested routes check
if(oRouteResult._asImports.length !== 0){

    //simple dump :)
    if(oRouteResult._asImports.length == 1){

        _priv.bodyElem.innerHTML = oRouteResult._asImports[0]._asImports[0];

        console.info('simple dump - overiding body wrapper');

        return;
    }

    //check for specific dump :)
    if(oRouteResult._asImports.length > 1){

        console.log(oRouteResult._asImports);

        for(var i = 0; i <oRouteResult._asImports.length; i++){

            var aImports = oRouteResult._asImports[i];

            //check nested route includes current screen
            if(i == 0 && aImports.sName == _priv.sPrevScreen){

                bRouteIncludesPrevScreen = true;

                continue;
            }

            //just dump sub route in container
            if(bRouteIncludesPrevScreen == true){

                document.querySelector('#body-container').innerHTML = aImports._asImports;

                console.log('Previous screen tracked... .');
            }

            //was not able to track prev screen - simple dump
            if(_priv.sPrevScreen == undefined){

                console.log(aImports);

                console.log('Was not able to track previous screen.');
            }
        }

        console.info('specific dump - overiding specific wrappers :)');

        return;
    }
}

console.log(oRouteResult._asImports.length);

//show 404 route 
_priv.bodyElem.innerHTML = _priv.o404Route;
};

//get route matching current url pathname
_priv.router = function(){

var sPathname = window.location.pathname,
    aPathnameSplit = sPathname.split('/');
    aPathnameSplit.shift();

//current location is home route
var bHomeRoute = false;

var oResult = {
    _asImports: [],
    oRoute: {}, // contain nested route to interate?
};

//get home route
if(aPathnameSplit[aPathnameSplit.length -1] == ""){
    bHomeRoute = true;
}

//search routes
for(var route in oRoutes){

    var oPath = {
        sName: "",
        _asImports: ""
    };

    if(aPathnameSplit.length > 1){

        //deep route search 
        _priv.getNestedRoutes(oRoutes[route], aPathnameSplit, oResult);
    
    }else{

        //surface level route search
        if(bHomeRoute == true){

            oPath.sName = '/';
            oPath._asImports = oRoutes[route]._asImports;

            oResult._asImports.push(oPath); 

            //track parent screen to help determine sub dumps :)
            _priv.sPrevScreen = oPath.sName;
        
        }else{

            //second level route search
           for(var screen in oRoutes[route]){

                if(screen == aPathnameSplit[0]){

                    var matchedRoute = oRoutes[route][screen];

                    var oPath = {
                        name: screen,
                        _asImports: matchedRoute._asImports
                    };

                    oPath.sName = screen;
                    oPath._asImports = matchedRoute._asImports;
    
                    oResult._asImports.push(oPath); 

                    //track parent screen to help determine sub dumps :)
                    _priv.sPrevScreen = oPath.sName;

                    break;
                }
            }
        }
    }
}

//render supported route or 404 page
_priv.renderRouteImports(oResult);
};

//contents loaded listener
document.addEventListener('DOMContentLoaded', function(){

_priv.sPrevRoute = window.location;

_priv.bodyElem = document.querySelector('body');

////get 404 route so it can it easily referenced
_priv.get404Route();

document.body.addEventListener('click', _events.RouteLinkClick);

_priv.router();
});

//browser back and foward button listener
window.addEventListener('popstate', _priv.router);