
//WILL LIKELY BE MOVED TO PROJECT'S FETCH WRAPPER
var request = function request(req, res, jsonSubmit){

    // Utitlit function to handle checking HTTP return status
    function checkStatus(response) {

        if (response.status >= 200 && response.status < 300) {

            return response;

        }
        else {

            journal.log({ type: 'error', owner: 'UI', module: 'emp', submodule: 'fetchWrapper', func: 'checkStatus' }, response.statusText);

            var error = new Error(response.statusText);

            error.response = response;

            throw error;
        }
    }

    // Utility function to parse JSON
    function parseJSON(response) {

        console.log(response);

        if (typeof response === "object") {

            return response.json();
        }
        else if (typeof response === "string") {

            try {

                return JSON.parseJSON(response);
            }
            catch (e) {
                return response;
            }

        }

        return response;
    }

    function encodeFormData(oData) {

        var aEncodedValues = [];

        for (var key in oData) {
            aEncodedValues.push(encodeURIComponent(key) + '=' + encodeURIComponent(oData[key]));
        }

        return aEncodedValues.join('&');
    }

    if (req.url) {

        var fetchObj = {
            cache: 'no-cache',
            method: req.method,
        };

        if ((location.hostname === "localhost" || location.hostname === "127.0.0.1") /*&& location.port === "8888" && !req.ignoreLocal*/) {

            //journal.log({ type: 'warning', owner: 'UI', module: 'emp', submodule: 'ajax', func: 'request' }, 'Request method being forced to "GET"');
            console.warn('Request method being forced to "GET"');

            req.method = 'GET';
        }
        else {

            // Add method only if needed
            if (!req.method) {
                fetchObj.method = 'POST';
            }
            else {
                fetchObj.method = req.method;
            }

            if (!req.headers) {

                // Add this in all other environments not UI developer machines
                fetchObj.mode = 'cors';
                fetchObj.credentials = 'same-origin';

                fetchObj.headers = {
                    'Access-Control-Allow-Origin': '*'
                };
            }
            else {

                fetchObj.mode = 'cors';
                fetchObj.headers = req.headers;
            }

            if (req.body && typeof req.body === "object" && jsonSubmit) {

                req.data = JSON.stringify(req.body);
            }

            //HOW WILL ROUTE'S CONTENT BE RETURN?
            if (jsonSubmit) {

                if (fetchObj.headers) {
                    fetchObj.headers['Content-Type'] = 'application/json';
                }

                fetchObj.body = req.data;

            }
            else {

                if (fetchObj.headers) {
                    fetchObj.headers['Content-Type'] = 'application/x-www-form-urlencoded';
                }

                fetchObj.body = encodeFormData(req.data);
            }

        }

        //Make request
        fetch(req.url, fetchObj)
        .then(checkStatus)
        .then(parseJSON)
        .then(function(data) {

            if (typeof res === "object") {

                // If response contains a done function execute it
                if (res.done) {

                    res.done(data);
                }

            }
            else {

                //journal.log({ type: 'error', owner: 'UI', module: 'fetch', func: 'irequest' }, 'Fetch completed but, there was no response object!');
                console.error('Fetch completed but, there was no response object!');
            }
        })
        //resolved or rejected - always
        .then(function(data) {

            if (res.always) {

                res.always(data);
            }

        })
        // Same as jQuery fail!
        .catch(function(err) {

            //journal.log({ type: 'error', owner: 'UI', module: 'fetch', func: 'irequest' }, 'Fetch Data request failed!');
            console.error('Fetch Data request failed!');


            if (res.fail) {

                res.fail(err);
            }

        });;
    }
};

//sets active data attribute on target
_priv.setActiveRoute = function(dTarget){

    //was route active before current click
    var bRouteViewActive = true;

    var dActiveRoute = document.querySelector('[data-route-active]');

    if(dTarget !== dActiveRoute){

        dActiveRoute.removeAttribute('data-route-active');

        dTarget.setAttribute('data-route-active', '');
        
        //IS THIS NEEDED?
        return bRouteViewActive = false;
    }

    //IS THIS NEEDED?
    return bRouteViewActive; 
};

/*
    TEMP FAKE ROUTES 
*/
var aoRoutes = [
    {
        route: "/",
        view: function(){ console.log('Viewing Parcel Home')}//"<p>Home</p><i>This is the <b>home</b> route;s content.<i/>"
    },
    {
        route: "/parcel-size-location",
        view: function(){ console.log('Viewing Parcel Size and Location')}//"<p>About</p><i>This is the <b>about us</b> route's content.<i/>"
    },
    {
        route: "/parcel-permits-deeds",
        view: function(){ console.log('Viewing Parcel Permits and Deeds')}//"<p>Contact Us</p><i>This is the <b>contact us</b> route's content.<i/>"
    },
    {
        route: "/owners",
        view: function(){ console.log('Viewing Owners')}//"<p>Contact Us</p><i>This is the <b>contact us</b> route's content.<i/>"
    }
];

//navigates page
_priv.route = function(){

};

//Fetch content of target
_priv.fetchRouteContent = function(oTarget){

    var routeExist = false;

    var req = {
        url: oTarget.url
    };

    var res = {
        "done": function(data){

            console.log(data);
        },
        "fail": function(err){

            console.log(err);
        }
    };

    //fetch route's content
    //request(req, res);

    for(var r = 0; r < aoRoutes.length; r++){

        var route = aoRoutes[r];
        
        //update browser session history stack
        history.pushState(null, null, oTarget.url);

        //check target url matches route
        if(route.route == location.pathname){

            route.view();

            routeExist = true;

            break;
        }

    }

    //route not found - DO WE NEED 404 ERROR PAGE or JUST A PAGE LEVEL ERROR MESSAGE STATING ROUTE NOT FOUND?
    if(routeExist == false){

        console.error('ROUTE NOT FOUND...GENERATING 404 ERROR PAGE');
    }

    console.log('FETCHING ' + oTarget.title + ' CONTENT');
};

/* 
    EVENTS 
*/

//route links click listener
_events.RouteLinkClick = function(){

    document.body.addEventListener('click', function(evt){

        var dTarget = evt.target;

        if(dTarget.matches('[data-route]')){

            evt.preventDefault();

            oTarget.title = dTarget.innerText;
            oTarget.url = dTarget.href;

            //mark target as active and inform if target's view was active before click
            var bRouteViewActive = _priv.setActiveRoute(dTarget);
            
            //fetch route content if not current view - IS THIS NEEDED
            //if(bRouteViewActive == false){

                _priv.fetchRouteContent(oTarget);
            //}
        }
    });
};

_priv.buildPath = function(){

    //request route resources path
    var oResourcesPath = {
        root: {},
        resource: "root"
    };

    var createNestedObject = function(arr, pathObj){
  
        var reference = pathObj.root;
        
        //index 0 is blank when pathname is split
        var i = 1;
        
        while(i < arr.length) {
        
            const pathName = arr[i];
          
            reference.resource = pathName;
          
            reference[pathName] = {};
          
            reference = reference[pathName];
          
            i++;
        };
        
        return pathObj;
      
      };

    if(_priv.activeRoute.url !== ''){

        var oActiveRouteURL = new URL(_priv.activeRoute.url);

        //app and sub pages request
        if(oActiveRouteURL.pathname !== '/'){

            var aActiveRoutePathnameSplit = oActiveRouteURL.pathname.split('/');

            oResourcesPath = createNestedObject(aActiveRoutePathnameSplit, oResourcesPath);
        }
    }

    //console.log(oActiveRouteURL);
    console.log(oResourcesPath);

    return oResourcesPath;
};


var router = function(){

    //route links listener
    _events.RouteLinkClick();
};

var bSurfaceRouteFound = false;

var r = 1;

//surface level matching
while(r < aPathnameSplit.length){

    var sPath = aPathnameSplit[r];

    //home route
    if(r == 1 && sPath == ''){
        sPath = '/';
    }

    //matching route
    for(var route in routes){

        if(sPath == route || ('/' + sPath) == route){

            oModules[route] = routes[route];

            bSurfaceRouteFound = true;

            break;
        }
    }

    //deep search - nested route
    if(aPathnameSplit.length - 1 > 1){

        _priv.iterateRoute(oModules, aPathnameSplit.slice(2));
    }

    if(bSurfaceRouteFound){
        break;    
    }

    r++;
}

//routes page based on url
/* var navigateTo = function(url){

    //'adds an entry to the browser's session history stack' - MDN
    history.pushState(null, null, url);

    router();
}; 

//browser back and forward listener
window.addEventListener('popstate', router);
*/


