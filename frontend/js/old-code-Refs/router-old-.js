/* 
    GLOBAL VARIABLES 
*/

//TEMP IMPORT - DELETE
var aoRoutesRegistery = {
    "/": {
        "_module": `<header>
                        <h1>CUI2 - LANDING PAGE</h1>
                        <hr>
                    </header>
                    <main>
                        <h2>Apps List</h2>
                        <div id="body-container">
                        <nav>
                            <ul>
                            <li><a href="/" title="CUI2 Landing Home" data-route>Home</a></li>
                            <li><a href="/parcel" title="Parcel" data-route>Parcel</a></li>
                            <li><a href="/reports" title="Reports" data-route>Reports</a></li>
                            </li>
                            </ul>
                        </nav>
                        </div>
                    </main>`
    },
    "parcel":{
        "_module": `<header>
                        <h1>CUI2 - PARCEL LANDING PAGE</h1>
                        <li><a href="/" title="CUI2 Landing Home" data-route>Home</a></li>
                        <hr>
                    </header>
                    <main>
                        <h2>Parcel - 113.-1-20.201</h2>
                        <div id="body-container">
                            <nav>
                            <ul>
                                <li><a href="/parcel/owners" title="Parcel-Owners" data-route>Owners</a></li>
                                <li><a href="/parcel/search" title="Search" data-route>Search</a></li>
                            </li>
                            </ul>
                            </nav>
                        </div>
                    </main>`,
                "owners": {
                    "_module": `<table>
                                    <tr>
                                    <th>Company</th>
                                    <th>Contact</th>
                                    <th>Country</th>
                                    </tr>
                                    <tr>
                                    <td>Alfreds Futterkiste</td>
                                    <td>Maria Anders</td>
                                    <td>Germany</td>
                                    </tr>
                                    <tr>
                                    <td>Centro comercial Moctezuma</td>
                                    <td>Francisco Chang</td>
                                    <td>Mexico</td>
                                    </tr>
                                    <tr>
                                    <td>Ernst Handel</td>
                                    <td>Roland Mendel</td>
                                    <td>Austria</td>
                                    </tr>
                                    <tr>
                                    <td>Island Trading</td>
                                    <td>Helen Bennett</td>
                                    <td>UK</td>
                                    </tr>
                                    <tr>
                                    <td>Laughing Bacchus Winecellars</td>
                                    <td>Yoshi Tannamuri</td>
                                    <td>Canada</td>
                                    </tr>
                                    <tr>
                                    <td>Magazzini Alimentari Riuniti</td>
                                    <td>Giovanni Rovelli</td>
                                    <td>Italy</td>
                                    </tr>
                                </table>`
                },
                "search": {
                    "_module": `<form>
                                    <input type="text" placeholder="Search parcels..." name="search-parcels">
                                    <button type="submit">Search</button
                                </form>`
                    
                }
    },
    "reports": {
            "_module": `<header>
                            <h1>CUI2 - REPORTS LANDING PAGE</h1>
                            <hr>
                        </header>
                        <main>
                            <h2>Apps List</h2>
                            <div id="body-container">
                                <table>
                                    <tr>
                                        <th>ID</th>
                                        <th>Filed</th>
                                        <th>Processed</th>
                                    </tr>
                                    <tr>
                                        <td>0123456789</td>
                                        <td>Yes</td>
                                        <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>987654321</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>0123456789</td>
                                        <td>Yes</td>
                                        <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>987654321</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>0123456789</td>
                                        <td>Yes</td>
                                        <td>No</td>
                                    </tr>
                                    <tr>
                                        <td>987654321</td>
                                        <td>No</td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </main>`
    }
};

var _priv = {
        activeRoute:{
            title: "",
            url: ""
        }   
    },

    _events = {},

    _attrs = {
        //route links with this attr
        route: "data-route",
        activeRoute: "data-route-active"
    },

    _ids = {
        routeViewElem: "body-container"
    },

    //clicked route target - link
    oTarget = {
        title: "",
        url: ""
    },

    //cache previous requested contents?
    oFetchedContentCache = {};

//render route's view
_priv.renderRouteView = function(evt, oViewContent){

    if(oViewContent.routeExist){

        if(_priv.dRouteViewWrapper){

            //Update page title
            document.title = _priv.activeRoute.title;

            //append routes content
            _priv.dRouteViewWrapper.innerHTML = oViewContent.result.view;
        }

        oViewContent.result.log();
    }
};

_priv.findRoute = function(path){

    //array of nested pathnames
    if(typeof path == 'object'){

        //index 0 is always blank - start at 1
        var aPath = path.slice(1);

        var i = 0; 

        while(i < aPath.length){

            /* for(route in aoRoutesRegistery){

                
            } */

            console.log(aoRoutesRegistery[aPath[i]]);

            return aoRoutesRegistery[aPath[i]]._module;

            i++;
        }
    }

    //single pathname string - landing page - [/]
    for(route in aoRoutesRegistery){
        
        if(path == route){

            return aoRoutesRegistery[route]._module;
        }
    }

};

//Fetch route contents
_priv.getRouteContent = function(evt){

    if(_priv.activeRoute.url !==''){
        
        var oActiveRouteURL = new URL(_priv.activeRoute.url),
            routePath = (oActiveRouteURL.pathname == '/') ? oActiveRouteURL.pathname : oActiveRouteURL.pathname.split('/');

        var routeContent = _priv.findRoute(routePath);

        document.querySelector('body').innerHTML = routeContent;
    }

    console.log('FETCHING ' + _priv.activeRoute.title + ' CONTENT');
};

//sets active data attribute on target
_priv.setActiveRoute = function(evt, dTarget){

    if(dTarget){

        var dActiveRoute = document.querySelector('[data-route-active]');

        if(dTarget !== dActiveRoute){

            dActiveRoute.removeAttribute('data-route-active');
    
            dTarget.setAttribute('data-route-active', '');

            _priv.setActiveRouteRefValues(dTarget.innerText, dTarget.href);
        }

    }else{

        //links to route
        _priv.daRouteLinks = document.querySelectorAll('[data-route]');

        var r = 0;

        while(r < _priv.daRouteLinks.length){

            var route = _priv.daRouteLinks[r];

            //set to active
            if(window.location.href == route.href){

                route.setAttribute(_attrs.activeRoute, '');

                _priv.setActiveRouteRefValues(route.innerText, route.href);
            }

            r++
        }
    } 
};

_priv.setActiveRouteRefValues = function(title, url){
    _priv.activeRoute.title = title;
    _priv.activeRoute.url = url;
};

//route links click listener
_events.RouteLinkClick = function(evt, dTarget){

    var dTarget = evt.target;

    if(dTarget.matches('[data-route]')){

        evt.preventDefault();

        //update active route details
        _priv.setActiveRouteRefValues(dTarget.innerText, dTarget.href);

        //_priv.setActiveRoute(evt, dTarget);
        
        //update browser session history stack
        history.pushState(null, null, _priv.activeRoute.url);
        
        //DO WE PREVENT REQUEST IF ROUTE IS ALREADY ACTIVE?
        _priv.router(evt, dTarget);
    }
};

_priv.router = function(evt, dTarget){

    //route view elems
    _priv.dRouteViewWrapper = document.querySelector('#' + _ids.routeViewElem);

    //links to route
    _priv.daRouteLinks = document.querySelectorAll('[data-route]');

    //_priv.setActiveRoute(evt, dTarget);

    //fetch current route content
    _priv.getRouteContent(evt);
};

//document content load listener
document.addEventListener('DOMContentLoaded', function(evt, dTarget){
    _priv.router(evt, dTarget);
    //_priv.setActiveRoute(evt, dTarget);
});

//browser back and forward listener
window.addEventListener('popstate', function(evt, dTarget){
    _priv.router(evt, dTarget);
});

//route links listner
document.body.addEventListener('click', function(evt, dTarget){
    _events.RouteLinkClick(evt, dTarget);
});