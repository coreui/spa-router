import {oRoutes} from'../js/routes';

/*
    * DEFAULT ROUTER CLASS
*/
export default class Router{

    constructor(routesObject){

        this.oRoutes = routesObject;
        
        //route links with this attr
        this._sRoute = "[data-route]";
        this._sActiveRoute =  "[data-route-active]";

        this._sPrevRoute = undefined;
        this._dBodyElem = document.querySelector('body');

        //init
        this.initialize();
    }

    initialize(){
        
        //contents loaded listener
        document.addEventListener('DOMContentLoaded', ()=>{
            
            //get 404 route so it can it easily referenced
            this.get404Route();

            //route links click listener
            document.body.addEventListener('click', this.routeLinkClick);

            this.router();
        });

        //browser back and foward button listener
        window.addEventListener('popstate', this.router);
    }

    //get 404 route
    get404Route(){

        var oRoutes = this.oRoutes;

        for(var route in oRoutes){
            
            //second level route search
            for(var screen in oRoutes[route]){
    
                if(screen == '404'){
    
                    var o404Route = oRoutes[route][screen];
    
                    return o404Route._asImports;
                }
            }
        }
    };

    //route link click listener
    routeLinkClick(evt){

        var dTarget = evt.target;

        if(dTarget.matches(this._sRoute)){

            evt.preventDefault();

            this._sPrevRoute = dTarget.href;
            
            this.nagivate(dTarget.href);
        }
    };

    nagivate(url){
    
        //update browser session history stack
        history.pushState(null, null, url);
        
        this.router();
    };

    //get route matching current url pathname
    router(){

        var oRoutes = this.oRoutes;

        var sPathname = window.location.pathname,
            aPathnameSplit = sPathname.split('/');
            aPathnameSplit.shift();

        //current location
        var bHomeRoute = false;

        var oResult = {
            _asImports: [],
            oRoute: {}, // contain nested route to interate?
        };

        //get home route
        if(aPathnameSplit[aPathnameSplit.length -1] == ""){
            bHomeRoute = true;
        }

        //search routes
        for(var route in oRoutes){

            var oPath = {
                sName: "",
                _asImports: ""
            };

            if(aPathnameSplit.length > 1){

                //deep route search 
                this.getNestedRoutes(oRoutes[route], aPathnameSplit, oResult);
            
            }else{

                //surface level route search
                if(bHomeRoute == true){

                    oPath.sName = '/';
                    oPath._asImports = oRoutes[route]._asImports;

                    oResult._asImports.push(oPath); 

                    //track parent screen to help determine sub dumps :)
                    this._sPrevScreen = oPath.sName;
                
                }else{

                    //second level route search
                    for(var screen in oRoutes[route]){

                        if(screen == aPathnameSplit[0]){

                            var matchedRoute = oRoutes[route][screen];

                            var oPath = {
                                name: screen,
                                _asImports: matchedRoute._asImports
                            };

                            oPath.sName = screen;
                            oPath._asImports = matchedRoute._asImports;
            
                            oResult._asImports.push(oPath); 

                            //track parent screen to help determine sub dumps :)
                            this._sPrevScreen = oPath.sName;

                            break;
                        }
                    }
                }
            }
        }

        console.log('Matched route(s):' ,oResult._asImports);

        //render supported route or 404 page
        this.renderRouteImports(oResult);
    };

    //deep route search
    getNestedRoutes(routesObject, aPathnames, oResult){

        var n = 0;
    
        var findNestedRoute = function(routesObject){
    
            for(var route in routesObject){
    
                if(route == aPathnames[n] && typeof routesObject[route] == 'object'){
    
                    var oPath = {
                        sName: aPathnames[n],
                        _asImports: routesObject[route]._asImports
                    };
    
                    oResult._asImports.push(oPath);
    
                    n++;
    
                    findNestedRoute(routesObject[route]);
                }
            }
        };
        
        findNestedRoute(routesObject);
    };

    renderRouteImports(oRouteResult){

        console.log('Previous screen:',this._sPrevScreen);
    
        var bRouteIncludesPrevScreen = false;
    
        //nested routes check
        if(oRouteResult._asImports.length !== 0){
    
            //simple dump :)
            if(oRouteResult._asImports.length == 1){
    
                this._dBodyElem.innerHTML = oRouteResult._asImports[0]._asImports[0];
    
                console.info('simple dump - overiding body wrapper');
    
                return;
            }
    
            //check for specific dump :)
            if(oRouteResult._asImports.length > 1){
    
                console.log(oRouteResult._asImports);
    
                for(var i = 0; i <oRouteResult._asImports.length; i++){
    
                    var aImports = oRouteResult._asImports[i];
    
                    //check nested route includes current screen
                    if(i == 0 && aImports.sName == this._sPrevScreen){
    
                        bRouteIncludesPrevScreen = true;
    
                        continue;
                    }
    
                    //just dump sub route in container
                    if(bRouteIncludesPrevScreen == true){
    
                        document.querySelector('#body-container').innerHTML = aImports._asImports;
    
                        console.log('Previous screen tracked... .');
                    }
    
                    //was not able to track prev screen - simple dump
                    if(this._sPrevScreen == undefined){
    
                        console.log(aImports);
    
                        console.log('Was not able to track previous screen.');
                    }
                }
    
                console.info('specific dump - overiding specific wrappers :)');
    
                return;
            }
        }
    
        console.log(oRouteResult._asImports.length);
    
        //show 404 route 
        this._dBodyElem.innerHTML = this.get404Route();
    };
}

new Router(oRoutes);